-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-07-2020 a las 19:39:43
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `symfony`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Usuario_nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Roles` enum('ROLE_ADMIN','ROLE_USER') COLLATE utf8_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `Agregado` datetime NOT NULL,
  `Actualizar_agregado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `Usuario_nombre`, `Nombre`, `Apellido`, `email`, `Password`, `Roles`, `activo`, `Agregado`, `Actualizar_agregado`) VALUES
(1, 'erickormaza', 'Erick', 'Ormaza', 'erick@hotmail.com', 'erick', 'ROLE_ADMIN', 1, '2020-07-07 00:26:09', '2020-07-07 00:26:09'),
(2, 'abispichardo', 'abismar', 'pichardo', 'abismarpichardo@gmail.com', 'abismar', 'ROLE_USER', 1, '2020-07-07 00:57:00', '2020-07-07 00:57:00'),
(3, 'd32e', '23e22', '2e323', '23e2@gmail.com', '2e23e', 'ROLE_USER', 1, '2020-07-07 18:57:14', '2020-07-07 18:57:14'),
(4, 'd32e', '23e22', '2e323', '23e2@gmail.com', '2e23e', 'ROLE_USER', 1, '2020-07-07 18:57:43', '2020-07-07 18:57:43'),
(5, 'erick143', 'erick', 'ormaza', 'erick', 'ericke2', 'ROLE_USER', 1, '2020-07-07 19:17:48', '2020-07-07 19:17:48');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
