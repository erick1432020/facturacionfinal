<?php

namespace EOM\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuarioNombre')
            ->add('nombre')
            ->add('apellido')
            ->add('email', 'email')
            ->add('password', 'password')
            ->add('roles','choice', array('choices' => array('ROLE_ADMIN' => 'Administrator', 'ROLE_USER' => 'User')))
            ->add('activo','checkbox')
            ->add('save', 'submit', array('label'=> 'Guardar Usuario'))
        
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EOM\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }
}
