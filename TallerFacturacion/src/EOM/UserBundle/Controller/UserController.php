<?php

namespace EOM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EOM\UserBundle\Entity\User;
use EOM\UserBundle\Form\UserType;

class UserController extends Controller
{
    public function indexAction()
    {
    
      $em = $this->getDoctrine()->getManager();
      $users = $em->getRepository('EOMUserBundle:User')->findAll();
      /*
      $res = 'Lista de Usuarios: <br/>';
      foreach ($users as $user){
        $res .= 'Usuario:' . $user->getUsuarioNombre() . ' - Email: ' .$user->getEmail () . '<br />';
      }

      return new Response($res);
      */
      return $this->render('EOMUserBundle:User:index.html.twig', array('users' => $users));
    }
    public function addAction(){

        $user = new User();
        $form = $this->createCreateForm($user);
        return $this->render('EOMUserBundle:User:agregar.html.twig', array('form'=>$form->createView()));
    }
   private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
                'action' => $this->generateUrl('eom_user_create'),
                'method' => 'POST'
            ));
        
        return $form;
    }
    public function createAction(Request $request)
    {   
        $user = new User();
        $form = $this->createCreateForm($user);
        $form->handleRequest($request);
        
        if($form->isValid())
        {  
          
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                //return $this->redirectToRoute('eom_user_index');  
                return $this->redirect($this->generateUrl('eom_user_index'));              
        }
        return $this->render('EOMUserBundle:User:agregar.html.twig', array('form' => $form->createView()));
    }

    public function viewAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('EOMUserBundle:User');
        $user = $repository->find($id);
        return new response('Usuario: ' . $user->getUsuarioNombre() . ' con email: ' . $user->getEmail() );
    }

        
}
    

        

   
