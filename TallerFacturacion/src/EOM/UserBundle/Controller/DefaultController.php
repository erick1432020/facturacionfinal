<?php

namespace EOM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EOMUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
